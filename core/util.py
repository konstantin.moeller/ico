import numpy as np
import matplotlib.pyplot as pl
import progressbar

from core.controller import Arena
from core.network import ICONet

def to_db(input):
    return 10*np.log10((np.abs(input+1e-5)**2).mean(-1))

FS = 24000                            # samples per second

##########################
# function parameters
# --------------
# sources      - list of tuples specifying the location of the sources relative to x_0
# layout       - list of tuples specifying the location of the microphones relative to x_0
# inputs       - name of the converted audiofile in the "data" folder
# scale        - scaling factor (value by which all distances get multiplied)
# T_max        - maximum runtime in seconds
# shielding    - boolean value indicating if shielding is active
# lr           - learning rate
# plot         - boolean value indicating if results get plotted
# verbose      - boolean value indicating if misc. info gets printed
# print_file   - boolean value indicating if plots get saved to file
# cutoff_freqs - value of the cutoff frequency of the filter for each microphone in order of declaration, last one is always the reference microphone
# name         - string id of current experiment, identifies plots saved to file
# rand         - boolean value indicating if weight vector is randomly initialized
# 
# return values
# --------------
# snrs      - array of noise reduction values over time
# audio     - byte array of noise reduced x_0 signal
# reference - byte array of original x_0 signal
##########################
def ico_noise_cancellation(sources, layout, inputs, scale, cutoff_freqs, T_max=4, shielding=False, lr=1e-7, plot=True, verbose=True, print_file=True, cutoff=None, name="test", rand=False, drift=False, drift_start=10, drift_end=11, transient=False):
    
    if plot:
        pl.figure(figsize=(10, 10), facecolor='white')
        pl.title("Simulation setup")
        pl.xlim(-3*scale,3*scale)
        pl.ylim(-3*scale,3*scale)
        pl.plot(0, 0, 'ko')
        pl.plot(sources[:, 0]*scale, sources[:, 1]*scale, 'wo', markeredgecolor='k')
        for l in layout:
            pl.plot(l[0]*scale, l[1]*scale, marker='o')
        pl.xlabel("Distance to reference microphone in x-direction [m]")
        pl.ylabel("Distance to reference microphone in y-direction [m]")
        if print_file:
            pl.savefig("imgs/"+name+"_setup.png", format="png")
            pl.savefig("imgs/"+name+"_setup.svg", format="svg")
        pl.show()

    # max simulation time per batch, convert times to num samples
    T_max = int(FS*T_max)
    # number of microphones
    N = layout.shape[0]

    arena = Arena(sources, layout, inputs, scale, cutoff_freqs, T_max=T_max, shielding=shielding, drift=drift, drift_start=drift_start, drift_end=drift_end, transient=transient)

    if rand:
        w_init = np.random.rand(N)
    else:
        w_init = np.zeros(N)

    w_hist = np.zeros([T_max, N])
    v_hist = np.full(T_max, np.nan)
    net = ICONet(w_init, lr)
    
    subs = 500

    u = np.zeros(N)            
            
    # calculate greatest microphone delay, so we don't run out of bounds
    min_delay = int(abs(np.min(arena.delays)))
    for t in progressbar.progressbar(range(min_delay+2, T_max-min_delay)):
        for j in range(N):
            u[j] = arena.in_data[j, t-1 + min(int(arena.min_idx[j]), 0)]
        net.update(u, (arena.effective_noise[t-1]-arena.effective_noise[t-2]))
        
        w_hist[t, :] = net.W
        v_hist[t]    = net.v
        arena.update(t, w_hist[t])
      
    audio = np.int16(arena.effective_noise).tobytes()
    reference = np.int16(arena.reference).tobytes()
    snrs = to_db(arena.reference.reshape([-1, FS//5])) - to_db(arena.effective_noise.reshape([-1, FS//5]))
    
    if verbose:
        print("Sample delays:\n", arena.delays)
        print("Learned weights:\n", net.W)
        print("Noise:", arena.effective_noise[t])
        print("Mean noise reduction:", np.mean(snrs))
        
    if plot:
        pl.figure(figsize=(20, 5), facecolor='white')
        pl.plot(np.arange(0, T_max/FS, (T_max/FS)/len(snrs)), snrs)
        pl.xlabel("time [s]")
        pl.ylabel("noise reduction [dB]")
        if print_file:
            pl.savefig("imgs/"+name+"_noise_reduction.png", format="png")
            pl.savefig("imgs/"+name+"_noise_reduction.svg", format="svg")
        pl.show()

        fig, axs = pl.subplots(4, 1, figsize=(20,15), facecolor='white')
        axs[0].plot(np.arange(0, T_max, subs)/FS, (arena.reference[::subs]))
        axs[0].set_title("mixed noise sources")

        axs[1].plot(np.arange(0, T_max, subs)/FS, arena.effective_noise[::subs])
        axs[1].set_title("effective noise")

        axs[2].plot(np.arange(0, T_max, subs)/FS, w_hist[::subs].reshape(int(T_max/subs), -1))
        axs[2].set_title("synaptic weights, learning rate = " + str(lr))
        
        axs[3].scatter(np.arange(0, T_max, subs)/FS, v_hist[::subs])
        axs[3].set_title("momentum evolution")
        axs[3].set_yscale('log')
        if print_file:
            pl.savefig("imgs/"+name+"_stats.png", format="png")
            pl.savefig("imgs/"+name+"_stats.svg", format="svg")
        pl.tight_layout()
        pl.show()
        
        if transient:
            pl.figure(figsize=(10, 5), facecolor='white')
            pl.plot(np.arange(0, T_max)/FS, arena.in_data[0]/2**15+0.5, 'b')
            pl.plot(np.arange(0, T_max)/FS, arena.in_data[1]/2**15, 'b')
            pl.plot(np.arange(0, T_max)/FS, arena.in_data[2]/2**15-0.5, 'b')
            pl.xlabel("runtime [s]")
            pl.yticks([0.5, 0, -0.5], ["x1", "x2", "x3"])
            pl.ylabel("microphone")
            if print_file:
                pl.savefig("imgs/"+name+"_microphone_waveforms.png", format="png")
                pl.savefig("imgs/"+name+"_microphone_waveforms.svg", format="svg")
            pl.show()
            
        del arena
        del net
            
    return snrs, audio, reference