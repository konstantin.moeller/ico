import numpy as np

class ICONet(object):
    def __init__(self, w_init, learning_rate):
        self.W = w_init.copy()
        self.v = 0
        self.learning_rate = learning_rate

    def update(self, u, du):
        beta = 0.9
        self.v = beta*self.v + (1-beta)*du                     # momentum
        self.W += self.learning_rate * (u.T * self.v)