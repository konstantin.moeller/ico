import numpy as np
import scipy
import pydub
import pydub.scipy_effects

V_SOUND = 343                              # speed of sound
FS      = 24000                            # samples per second

##########################
# function parameters
# --------------
# sources      - list of tuples specifying the location of the sources relative to x_0
# layout       - list of tuples specifying the location of the microphones relative to x_0
# inputs       - name of the converted audiofile in the "data" folder
# scale        - scaling factor (value by which all distances get multiplied)
# T_max        - maximum runtime in samples
# shielding    - boolean value indicating if shielding is active
# cutoff_freqs - value of the cutoff frequency of the filter for each microphone in order of declaration, last one is always the reference microphone
##########################
class Arena:
    def __init__(self, sources, layout, inputs, scale, cutoff_freqs, shielding=False, T_max=10000, drift=False, drift_start=10, drift_end=11, transient=False):
        self.noise_sources = sources
        self.sensors = layout
        self.inputs = inputs
        self.scale = scale
        self.shielding = shielding
        self.all_time = T_max
        self.cutoff_freqs = cutoff_freqs
        self.drift = drift
        self.drift_start = drift_start * FS
        self.drift_end = drift_end * FS
        self.transient = transient
        self.effective_noise = np.zeros(self.all_time, dtype=float)
        self.delays = np.zeros((len(inputs), layout.shape[0]), dtype=int)
        self.in_data = np.zeros([self.sensors.shape[0], self.all_time])
        self.reference = np.zeros([self.all_time])
        self.min_idx = np.zeros([layout.shape[0]])

        self.load_data()

    def load_data(self):
        assert len(self.cutoff_freqs) == self.sensors.shape[0] + 1
        
        for i in range(self.noise_sources.shape[0]):
            source = self.noise_sources[i]
            a = pydub.AudioSegment.from_mp3(self.inputs[i])
            if self.cutoff_freqs[-1] is not None:
                a = a.low_pass_filter(self.cutoff_freqs[-1])
            self.reference += np.array(a.get_array_of_samples()).reshape((-1, 2)).mean(axis=1)[:self.all_time] * (1/(np.linalg.norm(source)*self.scale))
            src_ref = round(FS*(np.linalg.norm(source)*self.scale)/V_SOUND)  
            for j in range(self.sensors.shape[0]):
                sens = self.sensors[j]
                src_sens = round(FS*(np.linalg.norm(source - sens)*self.scale)/V_SOUND)    # delay between source and sens in samples
                self.delays[i,j] = src_sens - src_ref                                      # delay between source and sens in samples relative to ref
                b = pydub.AudioSegment.from_mp3(self.inputs[i])
                if self.cutoff_freqs[j] is not None:
                    b = b.low_pass_filter(self.cutoff_freqs[j])
                signal = np.array(b.get_array_of_samples()).reshape((-1, 2)).mean(axis=1)
                if self.shielding and self.delays[i,j] > 0:
                    #self.in_data[j] += shift(signal, self.delays[i,j], 0)[:self.all_time] * 0.05
                    self.in_data[j] += np.zeros(self.all_time)
                else:
                    signal = shift(signal, self.delays[i,j], 0)[:self.all_time]
                    if self.drift == False:
                        signal *= (1/(np.linalg.norm(source - sens)*self.scale))
                        self.in_data[j] += signal
                    else:
                        weight_v = np.full(self.all_time, (1/(np.linalg.norm(source - sens)*self.scale)))
                        weight_v[self.drift_start:self.drift_end] = np.interp(np.arange(0, 1, 1/(self.drift_end-self.drift_start)), [0, 1], [weight_v[0], weight_v[0]+0.1*(-1**j)])
                        weight_v[self.drift_end:] = weight_v[0]+0.1*(-1**j)
                        signal = signal * weight_v
                        self.in_data[j] += signal
                    if self.transient:
                        transient = np.array(pydub.AudioSegment.from_mp3("siren_short.mp3").get_array_of_samples()).reshape((-1, 2)).mean(axis=1) * (1/(np.linalg.norm(source - sens)*self.scale))
                        #print(transient.shape[0])
                        self.in_data[j, (10+2*j)*FS:(10+2*j)*FS+transient.shape[0]] += transient * 0.125
                        
        for j in range(self.sensors.shape[0]):
            self.min_idx[j] = int(np.min(self.delays[:,j]))                                # pre-calculate delay to nearest source for performance reasons
                
    def update(self, t, weights):
        self.effective_noise[t] = self.reference[t]
        for j in range(self.sensors.shape[0]):
            self.effective_noise[t] -= weights[j] * self.in_data[j, t + min(int(self.min_idx[j]), 0)]

# https://stackoverflow.com/a/42642326
def shift(arr, num, fill_value=0):
    result = np.empty_like(arr)
    if num > 0:
        result[:num] = fill_value
        result[num:] = arr[:-num]
    elif num < 0:
        result[num:] = fill_value
        result[:num] = arr[-num:]
    else:
        result[:] = arr
    return result
